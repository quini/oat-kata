.PHONY: test

test:
	docker-compose up -d \
	&& docker-compose run --rm php sh -c 'composer install && ./vendor/bin/phpunit' \
	&& docker-compose down