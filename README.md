# OAT test

This is my attempt at resolving the OAT Test

## Install the project and dependencias

```
docker-compose up --build
```

## Run the tests

To execute the tests:

```
make test
```

## View the localhost (docs) with the description of the endpoints

```
http://localhost/api/doc
```