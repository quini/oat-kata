<?php
declare(strict_types=1);

namespace App\Tests\Util;

use App\Entity\Choice;
use App\Entity\Question;

final class QuestionFactory
{
    public static function getQuestion(): Question
    {
        $choices = [];
        $choice1 = new Choice();
        $choice1->setText('option 1');
        $choices[] = $choice1;
        $choice2 = new Choice();
        $choice2->setText('option 2');
        $choices[] = $choice2;
        $choice3 = new Choice();
        $choice3->setText('option 3');
        $choices[] = $choice3;
        $question = new Question();
        $question->setText('test 1');
        $question->setCreatedAt(new \DateTime());
        $question->setChoices($choices);

        return $question;
    }

    public static function getQuestionString(): string
    {
        return '{
          "text": "Test 2",
          "createdAt": "2021-07-18 22:15:37",
          "choices": [
            {
              "text": "choice 10"
            },
            {
              "text": "choice 20"
            },
            {
              "text": "choice 30"
            }
          ]
        }';
    }

    public static function getQuestionStringException(): string
    {
        return '{
          "text": "Test 2",
          "createdAt": "2021-07-18 22:15:37",
          "choices": [
            {
              "text": "choice 10"
            },
            {
              "text": "choice 20"
            }
          ]
        }';
    }
}