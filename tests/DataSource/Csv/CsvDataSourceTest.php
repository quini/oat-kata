<?php
declare(strict_types=1);

namespace App\Tests\DataSource\Csv;

use App\DataSource\Csv\CsvDataSource;
use App\Entity\Question;
use App\Entity\QuestionList;
use App\Tests\Util\QuestionFactory;
use PHPUnit\Framework\TestCase;

final class CsvDataSourceTest extends TestCase
{
    /**
     * @test
     */
    public function csv_data_source_get_question_list(): void
    {
        $csvDataSource = new CsvDataSource('data/questions_test.csv');

        $questionList = $csvDataSource->getQuestionList('es');

        self::assertInstanceOf(QuestionList::class, $questionList);
        self::assertIsArray($questionList->getData());
    }

    /**
     * @test
     */
    public function csv_data_source_post_question(): void
    {
        $question = QuestionFactory::getQuestion();
        $csvDataSource = new CsvDataSource('data/questions_test.csv');
        $questionListBefore = $csvDataSource->getQuestionList('es');

        $question = $csvDataSource->postQuestion($question);

        $questionListAfter = $csvDataSource->getQuestionList('es');

        self::assertInstanceOf(Question::class, $question);
        self::assertEquals(count($questionListBefore->getData()) + 1, count($questionListAfter->getData()));
    }
}