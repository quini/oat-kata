<?php
declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Question;
use App\Entity\QuestionList;
use App\Exception\InvalidChoiceException;
use App\Exception\InvalidDataException;
use App\Exception\InvalidNumberChoicesException;
use PHPUnit\Framework\TestCase;

final class QuestionListTest extends TestCase
{
    /**
     * @test
     */
    public function question_list_construct(): void
    {
        $questionList = new QuestionList();

        self::assertEmpty($questionList->getData());
    }

    /**
     * @test
     */
    public function question_list_set_get(): void
    {
        $question1 = new Question();
        $question2 = new Question();
        $data = [$question1, $question2];

        $questionList = new QuestionList();
        try {
            $questionList->setData($data);
        } catch (\Throwable) {
        }

        self::assertIsArray($questionList->getData());
        self::assertNotEmpty($questionList->getData());
    }

    /**
     * @test
     */
    public function question_list_set_true_empty_array(): void
    {
        $questionList = new QuestionList();

        try {
            $questionList->setData([]);
        } catch (\Throwable) {
        }

        self::assertIsArray($questionList->getData());
        self::assertEmpty($questionList->getData());
    }

    /**
     * @test
     */
    public function question_set_exception_by_type(): void
    {
        $question1 = new Question();
        $question2 = ['error'];
        $questions = [$question1, $question2];

        $questionList = new QuestionList();

        self::expectException(InvalidDataException::class);

        $questionList->setData($questions);
    }
}