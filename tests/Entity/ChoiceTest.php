<?php
declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Choice;
use PHPUnit\Framework\TestCase;

final class ChoiceTest extends TestCase
{
    /**
     * @test
     */
    public function choice_construct(): void
    {
        $choice = new Choice();

        self::assertEmpty($choice->getText());
    }

    /**
     * @test
     */
    public function choice_set_get(): void
    {
        $choice = new Choice();
        $choice->setText('choice 1');

        self::assertEquals('choice 1', $choice->getText());
    }
}