<?php
declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\Choice;
use App\Entity\Question;
use App\Exception\InvalidChoiceException;
use App\Exception\InvalidNumberChoicesException;
use PHPUnit\Framework\TestCase;

final class QuestionTest extends TestCase
{
    /**
     * @test
     */
    public function question_construct(): void
    {
        $question = new Question();

        self::assertEmpty($question->getText());
        self::assertNotEmpty($question->getCreatedAt());
        self::assertEmpty($question->getChoices());
    }

    /**
     * @test
     */
    public function question_set_get(): void
    {
        $today = new \DateTime();
        $choice1 = new Choice();
        $choice2 = new Choice();
        $choice3 = new Choice();
        $choices = [$choice1, $choice2, $choice3];

        $question = new Question();
        $question->setText('question 1');
        $question->setCreatedAt($today);
        try {
            $question->setChoices($choices);
        } catch (\Throwable) {
        }

        self::assertEquals('question 1', $question->getText());
        self::assertEquals($today, $question->getCreatedAt());
        self::assertIsArray($question->getChoices());
        self::assertNotEmpty($question->getChoices());
    }

    /**
     * @test
     */
    public function question_set_exception_by_choices_number(): void
    {
        $choice1 = new Choice();
        $choice2 = new Choice();
        $choices = [$choice1, $choice2];

        $question = new Question();

        self::expectException(InvalidNumberChoicesException::class);

        $question->setChoices($choices);
    }

    /**
     * @test
     */
    public function question_set_exception_by_type(): void
    {
        $choice1 = new Choice();
        $choice2 = new Choice();
        $choice3 = ['error'];
        $choices = [$choice1, $choice2, $choice3];

        $question = new Question();

        self::expectException(InvalidChoiceException::class);

        $question->setChoices($choices);
    }
}