<?php
declare(strict_types=1);

namespace App\Entity;

use App\Exception\InvalidChoiceException;
use App\Exception\InvalidNumberChoicesException;
use DateTime;

final class Question
{
    private string $text;

    private DateTime $createdAt;

    /**
     * @var Choice[]
     */
    private array $choices;

    public function __construct()
    {
        $this->text = '';
        $this->createdAt = new DateTime();
        $this->choices = [];
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTime $dateTime): self
    {
        $this->createdAt = $dateTime;

        return $this;
    }

    public function getChoices(): array
    {
        return $this->choices;
    }

    /**
     * @param array $choices
     * @return $this
     * @throws InvalidChoiceException
     * @throws InvalidNumberChoicesException
     */
    public function setChoices(array $choices): self
    {
        if (count($choices) !== 3) {
            throw new InvalidNumberChoicesException('The number of choices is incorrect. Valid number: 3');
        }

        if (!$this->validateChoices($choices)) {
            throw new InvalidChoiceException('The choices for Question is invalid. Only Choice objects allowed');
        }

        $this->choices = $choices;

        return $this;
    }

    private function validateChoices(array $choices): bool
    {
        $filterData = array_filter(
            $choices,
            function($item) { return $item instanceof Choice; }
        );

        return count($filterData) === count($choices);
    }
}