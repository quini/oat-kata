<?php
declare(strict_types=1);

namespace App\DataSource;

use App\Entity\Question;
use App\Entity\QuestionList;
use App\Exception\FileNotExistsException;
use App\Exception\InvalidDataException;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

abstract class AbstractDataSource
{
    public function __construct(private string $file)
    {
    }

    /**
     * @param string $lang
     * @return QuestionList
     */
    abstract public function getQuestionList(string $lang): QuestionList;

    /**
     * @param Question $question
     * @return Question
     * @throws FileNotExistsException
     * @throws InvalidDataException
     * @throws \App\Exception\InvalidChoiceException
     * @throws \App\Exception\InvalidNumberChoicesException
     * @throws \Throwable
     */
    public function postQuestion(Question $question): Question
    {
        try {
            $content = $this->getQuestionList('en');
            $data = $content->getData();
            $data[] = $question;

            $content->setData($data);

            $this->writeDataOnDataSource($this->file, $content);

        } catch (\Throwable $exception) {
            throw $exception;
        }

        return $question;
    }

    /**
     * @param string $file
     * @param QuestionList $questionList
     * @return mixed
     */
    abstract protected function writeDataOnDataSource(string $file, QuestionList $questionList);

    /**
     * @param QuestionList $questionList
     * @param array $encoders
     * @param string $format
     * @param array $context
     * @return string
     */
    protected function getDataToWrite(QuestionList $questionList, array $encoders, string $format, array $context = []): string
    {
        $normalizers = array(new DateTimeNormalizer(), new GetSetMethodNormalizer());

        $serializer = new Serializer($normalizers, $encoders);
        return $serializer->serialize(
            $questionList->getData(),
            $format,
            $context
        );
    }
}