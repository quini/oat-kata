<?php
declare(strict_types=1);

namespace App\DataSource\Json;

use App\DataSource\AbstractDataSource;
use App\DataSource\InterfaceDataSource;
use App\Entity\Choice;
use App\Entity\Question;
use App\Entity\QuestionList;
use App\Exception\FileNotExistsException;
use App\Exception\InvalidDataException;
use Stichoza\GoogleTranslate\GoogleTranslate;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class JsonDataSource extends AbstractDataSource implements InterfaceDataSource
{
    private const QUESTION_TEXT_HEADER = 'text';
    private const QUESTION_CREATED_AT_HEADER = 'createdAt';
    private const QUESTION_CHOICES_HEADER = 'choices';

    public function __construct(private string $file)
    {
        $this->file = sprintf('%s/%s', __DIR__, $file);
        parent::__construct($this->file);
    }

    /**
     * @param string $lang
     * @return QuestionList
     * @throws FileNotExistsException
     * @throws InvalidDataException
     * @throws \App\Exception\InvalidChoiceException
     * @throws \App\Exception\InvalidNumberChoicesException
     * @throws \ErrorException
     * @throws \JsonException
     */
    public function getQuestionList(string $lang): QuestionList
    {
        try {
            if (!file_exists($this->file)) {
                throw new FileNotExistsException('JSON file with data does not exists');
            }
            $content = file_get_contents($this->file);
            $content = json_decode($content, true, 512, JSON_THROW_ON_ERROR);

            return $this->fromArray($content, $lang);
        } catch (\Throwable $exception) {
            throw $exception;
        }
    }

    /**
     * @param string $file
     * @param QuestionList $questionList
     */
    protected function writeDataOnDataSource(string $file, QuestionList $questionList)
    {
        $encoders = array(new JsonEncoder());
        $format = 'json';
        $context = ['datetime_format' => 'Y-m-d H:i:s'];

        $result = $this->getDataToWrite($questionList, $encoders, $format, $context);

        file_put_contents($file, $result);
    }

    /**
     * @param array $data
     * @param string $lang
     * @return QuestionList
     * @throws InvalidDataException
     * @throws \App\Exception\InvalidChoiceException
     * @throws \App\Exception\InvalidNumberChoicesException
     * @throws \ErrorException
     */
    private function fromArray(array $data, string $lang): QuestionList
    {
        try {
            $translate = new GoogleTranslate();
            $translate->setSource();
            $translate->setTarget($lang);

            $questionList = new QuestionList();
            $questions = [];
            foreach ($data as $questionItem) {
                $question = new Question();
                $question->setText($translate->translate($questionItem[self::QUESTION_TEXT_HEADER]));
                $question->setCreatedAt(\DateTime::createFromFormat('Y-m-d H:i:s', $questionItem[self::QUESTION_CREATED_AT_HEADER]));
                $choices = [];
                foreach ($questionItem[self::QUESTION_CHOICES_HEADER] as $choiceItem) {
                    $choice = new Choice();
                    $choice->setText($translate->translate($choiceItem[self::QUESTION_TEXT_HEADER]));
                    $choices[] = $choice;
                }
                $question->setChoices($choices);
                $questions[] = $question;
            }
            $questionList->setData($questions);

            return $questionList;
        } catch (\Throwable $exception) {
            throw $exception;
        }
    }
}